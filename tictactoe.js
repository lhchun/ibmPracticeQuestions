//I am O, not X
//rowm column zero based

let data = [

    'OO?',
    'XOX',
    'XX?'


];

suggest(data);



function suggest(board){

    //listing winning conditions

    let horizontalr0 = [];
    let horizontalr1 = [];
    let horizontalr2 = [];

    let verticalc0 = [];
    let verticalc1 = [];
    let verticalc2 = [];

    let diagonalltr = [];
    let diagonalrtl = [];

    //horizontal initialization
    for (i = 0; i < board[0].length; i++){
        horizontalr0.push(board[0][i]);
    }


    for (i = 0; i < board[1].length; i++){
        horizontalr1.push(board[1][i]);
        
    }
    for (i = 0; i < board[2].length; i++){
        horizontalr2.push(board[2][i]);
        
    }

    //vertical initialization
    for (i = 0; i < board.length; i++){
        verticalc0.push(board[i][0]);
    }


    for (i = 0; i < board[1].length; i++){
        verticalc1.push(board[i][1]);
        
    }
    for (i = 0; i < board[2].length; i++){
        verticalc2.push(board[i][2]);
        
    }

    //diagonal initialization
    //Left to right diagonal
    for (i = 0; i < board.length; i++){
        diagonalltr.push(board[i][i]);
    }

    //right to left diagonal
    for (i = 2, r = 0; i > -1, r <board.length; i--, r++){
        diagonalrtl.push(board[r][i]);
    }

    console.log(diagonalrtl);

    //calculating winning strategy
    let r0 = calculateWin(horizontalr0);
    let r1 = calculateWin(horizontalr1);
    let r2 = calculateWin(horizontalr2);
    let c0 = calculateWin(verticalc0);
    let c1 = calculateWin(verticalc1);
    let c2 = calculateWin(verticalc2);
    let dLTR = calculateWin(diagonalltr);
    let dRTL = calculateWin(diagonalrtl);

    if (r0 == 2){
        if (horizontalr0.includes('X')){
            console.log();
        }
        else{
            return console.log('0, ' + horizontalr0.indexOf('?'));
        }
    }

    if (r1 == 2){
        if (horizontalr1.includes('X')){
            console.log();
        }
        else{
            return console.log('1, ' + horizontalr1.indexOf('?'));

        }
    }

    if (r2 == 2){

        if (horizontalr2.includes('X')){
            console.log();
        }
        else{
            return console.log('2, ' + horizontalr2.indexOf('?'));
        }
    }

    if (c0 == 2){
        if (verticalc0.includes('X')){
            console.log();
        }
        else{

            return console.log(verticalc0.indexOf('?') +  ' 0');
        }

    }
    if (c1 == 2){
        if (verticalc1.includes('X')){
            console.log();
        }
        else{

            return console.log(verticalc1.indexOf('?') +  ' 1');
        }

    }
    if (c2 == 2){
        if (verticalc2.includes('X')){
            console.log();
        }
        else{

            return console.log(verticalc2.indexOf('?') +  ' 2');
        }

    }
    if (dLTR == 2){
        if (diagonalltr.includes('X')){
            console.log();
        }
        else{

            return console.log(diagonalltr.indexOf('?') +", "+diagonalltr.indexOf('?'));
        }

    }

    if(dRTL == 2){
        if (diagonalrtl.includes('X')){
            console.log();
        }
        else if (diagonalrtl.indexOf('?') == 2){

            return console.log(diagonalrtl.indexOf('?') + ', 0');
        }
        else if (diagonalrtl.indexOf('?') == 1){

            return console.log(diagonalrtl.indexOf('?') + ', 1');
        }
        else if (diagonalrtl.indexOf('?') == 0){

            return console.log(diagonalrtl.indexOf('?') + ', 2');
        }
    }
    //calculating lossing avoidance strategy
    let r01 = calculateLoss(horizontalr0);
    let r11 = calculateLoss(horizontalr1);
    let r21 = calculateLoss(horizontalr2);
    let c01 = calculateLoss(verticalc0);
    let c11 = calculateLoss(verticalc1);
    let c21 = calculateLoss(verticalc2);
    let dLTR1 = calculateLoss(diagonalltr);
    let dRTL1 = calculateLoss(diagonalrtl);

    if (r01 == 2){
        if (horizontalr0.includes('O')){
            console.log();
        }
        else{
            return console.log('0, ' + horizontalr0.indexOf('?'));
        }
    }

    if (r11 == 2){
        if (horizontalr1.includes('O')){
            console.log();
        }
        else{
            return console.log('1, ' + horizontalr1.indexOf('?'));

        }
    }

    if (r21 == 2){

        if (horizontalr2.includes('O')){
            console.log();
        }
        else{
            return console.log('2, ' + horizontalr2.indexOf('?'));
        }
    }

    if (c01 == 2){
        if (verticalc0.includes('O')){
            console.log();
        }
        else{

            return console.log(verticalc0.indexOf('?') +  ' 0');
        }

    }
    if (c11 == 2){
        if (verticalc1.includes('O')){
            console.log();
        }
        else{

            return console.log(verticalc1.indexOf('?') +  ' 1');
        }

    }
    if (c21 == 2){
        if (verticalc2.includes('O')){
            console.log();
        }
        else{

            return console.log(verticalc2.indexOf('?') +  ' 2');
        }

    }
    if (dLTR1 == 2){
        if (diagonalltr.includes('O')){
            console.log();
        }
        else{

            return console.log(diagonalltr.indexOf('?') +", "+diagonalltr.indexOf('?'));
        }

    }
    if(dRTL1 == 2){
        if (diagonalrtl.includes('O')){
            console.log();
        }
        else if (diagonalrtl.indexOf('?') == 2){

            return console.log(diagonalrtl.indexOf('?') + ', 0');
        }
        else if (diagonalrtl.indexOf('?') == 1){

            return console.log(diagonalrtl.indexOf('?') + ', 1');
        }
        else if (diagonalrtl.indexOf('?') == 0){

            return console.log(diagonalrtl.indexOf('?') + ', 2');
        }
    }


    return console.log();
}

function calculateWin(input){

    count = 0;
    for (i = 0; i < input.length; i++){
        if(input[i] == 'O'){
            count++;
        }
    }
    return count;
}

function calculateLoss(input){

    count = 0;
    for (i = 0; i < input.length; i++){
        if(input[i] == 'X'){
            count++;
        }
    }
    return count;
}